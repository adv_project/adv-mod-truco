Motor del juego
===============

Estado del juego
----------------

```
phase: initializing|playing|paused|finished
players: [<user1> <user2> ... ]
players: 2|3|4|6
teamA: [<user> ...]
teamB: [<user> ...]
score_teamA: <N>
score_teamB: <N>
players_ok: [<user1> ...]
player_first: <username>
envido_team_A: <username>
envido_team_B: <username>
flor: true|false
flor_count: [flor flor contraflor_al_resto ...]
envido_count: [envido envido real_envido falta_envido]
flor_querida: true|false
envido_querido: true|false
flor_score_teamA: <N>
flor_score_teamB: <N>
envido_score_teamA: <N>
envido_score_teamB: <N>
flor_resolved: true|false
envido_resolved: true|false
hand_1_winner: teamA|teamB|tie
hand_2_winner: teamA|teamB|tie
hand_3_winner: teamA|teamB|tie
cards_played: [<card1> <card2> ...]
cards_played_hand_1: [<card1> <card2> ...]
cards_played_hand_2: [<card1> <card2> ...]
cards_played_hand_3: [<card1> <card2> ...]
current_hand: 1|2|3
```

Estado del jugador
------------------

```
state: room|spectator|playing|absent
game: <game_name>
hide_cards: true|false
plays_blind: true|false # Jugar 'tapado'
```

Programa
--------

El programa recibe el texto del chat de la mesa.

---

### El jugador tira una carta

- Si hay envido o flor pendiente, mostrar error. **?**
- Si hay envido o flor pendiente, mantenerlo pendiente incluso si se cambia de mano.
- Si todos han jugado su carta en esta mano, **cambiar de mano**.
- Si todos han jugado su carta y es la tercera mano o está definido el ganador, **terminar la mano**.

---

### El jugador canta

#### Envido

- Si no es la primera mano, mostrar error.
- Si hay real envido o falta envido cantado, mostrar error.
- Si hay flor cantada, mostrar error.
- Si no hay envido cantado y el jugador no es pie de su equipo, mostrar error.
- **Registrar envido.**

#### Real envido

- Si no es la primera mano, mostrar error.
- Si hay falta envido cantado, mostrar error.
- Si hay flor cantada, mostrar error.
- Si no hay envido o real envido cantado y el jugador no es pie de su equipo, mostrar error.
- **Registrar real envido.**

#### Falta envido

- Si no es la primera mano, mostrar error.
- Si hay falta envido cantado, mostrar error.
- Si hay flor cantada, mostrar error.
- Si no hay envido o real envido cantado y el jugador no es pie de su equipo, mostrar error.
- **Registrar falta envido.**

#### Flor

- Si no es la primera mano, mostrar error.
- Si hay contraflor cantada, mostrar error.
- Si hay flor cantada, **acumular puntos**.
- Si ya se ha jugado la carta en primera mano, mostrar error.
- **Registrar flor.**

#### Contraflor

- Si no es la primera mano, mostrar error.
- Si no hay flor cantada por el equipo contrario, **vale como flor**.
- Si hay flor cantada, **acumular puntos**.
- **Registrar flor**.

#### Contraflor al resto

- Si no es la primera mano, mostrar error.
- Si no hay flor o contraflor al juego cantada por el equipo contrario, mostrar error.
- **Registrar contraflor al resto.**

#### Contraflor al juego

- Si no es la primera mano, mostrar error.
- Si no hay flor cantada por el equipo contrario, mostrar error.
- **Registrar contraflor al juego.**

#### Son buenas

- Si no hay envido o flor pendiente, mostrar error.
- Si hay flor cantada por el equipo contrario, **registrar no quiero**.
- **Declarar ganador de envido o flor y asignar puntos.**

#### Truco

- Si hay envido o flor pendiente, mostrar error. **?**
- Si ya se ha cantado truco, mostrar error.
- **Registrar truco cantado.** _dejar pendiente si hay envido o flor pendiente_

#### Quiero retruco

- Si hay truco cantado por el equipo contrario, **registrar retruco**.
- Si no hay truco cantado, vale como **truco**.
- Si hay truco cantado por el equipo propio, mostrar error.
- Si hay envido o flor pendiente, dejar pendiente.

#### Quiero vale cuatro

- Si hay retruco cantado por el equipo contrario, **registrar vale cuatro**.
- Si no hay retruco cantado, vale como **quiero**.
- Si hay retruco cantado por el equipo propio, mostrar error.
- Si hay envido o flor pendiente, dejar pendiente.

#### No quiero

- Si hay truco, retruco, vale cuatro, contraflor o envido cantado por el equipo contrario, **registrar no quiero**.
- Si lo hay cantado por el equipo propio, mostrar error.
- En caso de truco, retruco o vale cuatro no querido, **terminar la mano**.

#### Quiero

- Si hay envido/real envido/falta envido o flor/contraflor cantado por el equipo contrario, **registrar quiero**.
- Si hay truco, retruco, vale cuatro o envido cantado por el equipo contrario, **registrar quiero**.
- Si hay envido/real envido/falta envido o flor/contraflor cantado por el equipo propio, mostrar error.
- Si no hay canto pendiente de quiero, mostrar error.
- **Registrar quiero.**

---

### El jugador canta un número

- Si no hay envido o flor querido y pendiente, desestimar.
- Si el jugador ya cantó sus puntos, mostrar error.
- Si el número no es válido, mostrar error.
- **Registrar puntos.**
- Si hay jugadores que aún no han cantado sus puntos, terminar.
- **Declarar ganador del envido o flor y asignar puntos.**

---

### Registrar envido o real envido

```
envido_cantado => <team>
envido_last => envido|real_envido
envido ? envido_aggr => +2
real_envido ? envido_aggr => +3
```

### Registrar falta envido

```
envido_cantado => <team>
envido_last => falta_envido
```

### Registrar flor o contraflor

```
flor_cantada => <team>
flor_last => flor|contraflor|...
flor ? flor_aggr => +3
```

### Registrar truco

### Registrar retruco

### Registrar vale cuatro

### Registrar quiero

### Registrar no quiero

### Registrar puntos de envido o flor

### Declarar ganador del envido o flor y asignar puntos

### Cambiar de mano

### Terminar la mano

