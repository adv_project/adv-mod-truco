# Truco

## Reglas

- Cada juego creado es una **sala**.
- Cada sala admite un máximo de _partidas_ creadas. Cada partida creada es una **mesa**.
- Todos los jugadores presentes en una _sala_ chatean entre sí.
- Todos los jugadores presentes en una _mesa_ chatean entre sí.
- Los chats asociados a la _sala_ y los asociados a las _mesas_ son mutuamente inaccesibles.
- Una excepción a la regla anterior se da cuando un usuario presente en una sala ingresa como espectador a una mesa. En tal caso, le es permitido leer la conversación y conocer el estado de la partida; las cartas recibidas por los jugadores le son inaccesibles. Este comportamiento es configurable por el creador de la mesa y por los moderadores de la sala.
- Cada _mesa_ creada espera que se unan `N` jugadores: 2, 3 (gallo), 4 o 6. El número de jugadores de cada mesa se asigna al crearse ésta, y no puede modificarse.
- Pasado el límite de tiempo establecido por el servidor, si en una _mesa_ no ingresó el número de jugadores necesarios, ésta se destruye.
- Durante una partida, las **palabras calientes** (_comandos_) son:
  - no quiero;
  - quiero;
  - son buenas;
  - envido/real envido/falta envido;
  - flor/contraflor/contraflor al resto/contraflor al juego;
  - truco/quiero retruco/quiero vale cuatro;
  - cualquier número o carta válidos;
  - me voy al mazo.
- _Veintiséis_ y _26_ son equivalentes.
- Los acentos y las mayúsculas se ignoran.
- Si no se cantó _truco_, _retruco_ cuenta como _truco_.
- Si no se cantó _flor_, _contraflor_ cuenta como _flor_.
- Cada sala tiene como mínimo un moderador, sin límite máximo. El moderador en jefe es el dueño del juego.
- El mod comunica a la mesa todo comando aceptado como válido.
- El mod comunica a la sala cada vez que un jugador entra a una mesa; este comportamiento es configurable en cada sala. Los jugadores pueden silenciar esta notificación.
- El mod comunica a la sala cada vez que una partida comienza o termina; este comportamiento es configurable en cada sala. Los jugadores pueden silenciar esta notificación.
- Los jugadores que participan de una partida pueden consultar anónimamente en todo momento el estado de la partida o de la mano en juego.
Los jugadores presentes en una sala pueden consultar anónimamente en todo momento el estado de las partidas en espera o en juego; los moderadores de la sala pueden configurar este comportamiento.
- Los comandos se pueden ingresar silenciosamente, de manera que la mesa vea sólo el resultado del comando, si lo hubiera.

## Implementación

- Las estadísticas de cada sala se almacenan en una base de datos (pgsql).
- El parseo de comandos se realiza mediante scripts en perl.
- La base de datos se actualiza al finalizar cada mano.
- La base de datos sólo acepta comandos preconcebidos, por cuestiones de seguridad; p. ej. `select do_update_*();`.
- Considerar la opción de jugar _sin flor_.
- Las cartas se pueden jugar _tapadas_. Una vez que se juega tapada una carta, se deberán jugar tapadas todas las cartas restantes hasta terminar la mano.
- Una vez que un jugador se sienta a una mesa, no puede retirarse hasta que la partida termina o la mesa es destruida.

## Comandos

- `:D <nombre>` Destruir una mesa. **NO**
- `:? [<comando>]` Mostrar la ayuda.
- `:E [<filtro/s>]` Muestra estadísticas.
- `:C` Configuración de usuario (general).

### Comandos válidos si se está en la sala

- `:c <nombre> <jugadores> [<con|sin>]` Crear una nueva mesa (tipo: 2, 3, 4 o 6, con/sin flor: opcional).
- `:e <nombre> [<equipo>]` Entrar a una mesa (equipo: a|b, equipo 'a' por defecto).
- `:v <nombre>` Entrar a una mesa como espectador.
- `:s` Ver el estado actual de la sala.

### Comandos válidos si se está en una mesa

- `@N`, `+N` Tirar la carta N (1, 2 o 3).
- `@@`, `::`, `++` Ver las cartas en mano.
- `:m` Ver el estado de la mano en juego.
- `:S` Salir de la mesa (sólo para espectadores).
- `:c` Configuración de usuario (para la mesa actual).
- `:t` Ver la tabla de la partida en juego.
- `:O` Ocultar las cartas al terminar la mano (no válido si se ha cantado envido o flor).
- `:T` Jugar tapado hasta terminar la mano.
- `:ok` Declarar que se está listo para comenzar.
- `:r` Redistribuir equipos (sin argumentos: redistribuye aleatoriamente los jugadores; con argumentos: agrupa los usuarios en el orden indicado -- todos los jugadores deben declarar ok nuevamente).
- `:R` Retirarse de la partida (válido sólo si la partida no ha comenzado).
- `! ...` Procesa los comandos sin mostrar el texto a los demás jugadores.

### Visualización de las tablas

```
> :m

Equipo 1: spiridon, fran_lopez
Equipo 2: mate_amargo, mlopezviedma

spiridon  | mate_amargo   | fran_lopez    | mlopezviedma
----------+---------------+---------------+----------------
5 copas   | 7 oros        | 6 espadas     | 10 copas
3 oros    |               |               |

> :t

Equipo 1: spiridon, fran_lopez
Equipo 2: mate_amargo, mlopezviedma

Equipo 1  | Equipo 2
----------+---------
12 malas  | 9 malas

```

