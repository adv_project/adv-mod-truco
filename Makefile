SHELL := /bin/bash
ifndef PREFIX
  PREFIX = /usr
endif
MOD := adv-truco
VERSION=$(shell git describe | rev | cut -d- -f2- | rev | sed -e 's/-/.r/' -e 's/^v//')

MDDIRS :=			.

EXECDIRS :=		hooks

PERLDIRS :=		lib \
							scripts

JSONDIRS :=		. \
							assets \
							assets/help

default:

.PHONY: clean

.FORCE:

install:
	for mddir in $(MDDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mddir; \
		for mdfile in $$mddir/*.md ; do \
			install -Dv -m 644 $$mdfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mdfile ; \
		done; \
	done
	for execdir in $(EXECDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execdir; \
		for execfile in $$execdir/* ; do \
			install -Dv -m 755 $$execfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execfile ; \
		done; \
	done
	for perldir in $(PERLDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perldir; \
		for perlfile in $$perldir/*.pl ; do \
			install -Dv -m 644 $$perlfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perlfile ; \
		done; \
	done
	for jsondir in $(JSONDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsondir; \
		for jsonfile in $$jsondir/*.json ; do \
			install -Dv -m 644 $$jsonfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsonfile ; \
		done; \
	done
	@sed -i $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/properties.json -e "s/[\"]version[\"]: [\"].*[\"]/\"version\": \"$(VERSION)\"/"

uninstall:
	@echo "Uninstalling..."
	@rm -r $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)

clean:

