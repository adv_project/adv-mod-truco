#!/usr/bin/env perl

# Construir y mostrar el banner.
sub showWelcomeBanner {
  my $player = shift;
  my @banner = [
    '',
    '        . - ^    ¬-    ..                       ',
    '     ,`                    \                    ',
    '     L    T R U C O ,      .^                   ',
    '     .⌐              ┬ ^                        ',
    '   ,    D I G O !    ²   ,╓ ╓                   ',
    '   ,              ` `   "▀▀▀▀▀*         ,       ',
    '    ` ¬~~-~=─¬_┴¬ ▄▄Ω▄▄└`H`^^╘ª██████▄«⌐ò@      ',
    '               " \'╓██▀          ▄██████▄▌▌W     ',
    '                  ▀▀▀▌},  ,▄▄█▓█████▀▀"▓▓-.     ',
    '                     `▀▀▀▀╫\╚`╜"▀└   ┘ ≤▄-      ',
    '                         "═⌐ ▐         ▌Ñ⌐\'     ',
    '                   ,-----⌐^  ¬ ¬-    \'ΓFÜ       ',
    '                     F`  Ä`    ,~ \' /`~kM       ',
    '               ┌ `\'L╢``` -   < , `<,∞^`╟╫ ²w    ',
    '        ¿FÜ"?  ⌡    ╨      └"ΦΦ▀~.τ⌐   ]Ü  ╞    ',
    '       /£- ¿Γ⌐╓     Γ            ƒ ▐¼╚╫██▌ y    ',
    '        ,`∩ \' ▐  ⌐ ╔        .-⌐  ╛÷-┘Γ ▀▀Γ  \   ',
    '     L \'r │ L ▐ ▓╛ ╣ -▓  £▓ ├╕╠ ≈█Γ╦^ ª+ä≤¬ ╒⌐  ',
    '    ¬¬¬¬╛≈>╘9íJ⌐,Jz╧`∩Éª¿╚-L╘╨▐F¥≤═"e╥╥╥╥⌐ ⁿ   ',
    ''
  ];
  callSendPlain($player, @banner);
  callSendInfo($player, "*** Bienvenido a la sala, ".$player."! ***");
}

sub welcomePlayer {
  my $player = shift;
  my @users = @{$ROOMSTATE->{'users'}};
  for my $i (0 .. (@users-1)) {
    my $u = $users[$i];
    if ($u eq $player) {
      showWelcomeBanner($u);
    } elsif (playerIsInRoom($u)) {
      callSendInfo($u, "*** ".$player." ha entrado a la sala ***");
    }
  }
}


1;
