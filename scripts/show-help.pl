#!/usr/bin/env perl

sub showHelp {
  my $player = shift;
  my $category = shift;
  my $command = shift;
  my $help_strings = readJsonFile($MODDIR."/assets/help/".$category.".json");
  my @help_message = @{$help_strings->{$command}};
  callSendPlain($player, dclone(\@help_message));
}

1;
