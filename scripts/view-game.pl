#!/usr/bin/env perl

# Registrar el jugador como espectador de la mesa.
# Notificar a la mesa y al jugador.
sub viewGame {
  my $player = shift;
  my $game = shift;
  callSendInfo($player, "*** Entras al juego '".$game."' como espectador (EN CONSTRUCCION) ***");
}

1;
