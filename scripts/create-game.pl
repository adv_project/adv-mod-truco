#!/usr/bin/env perl

# Copiar las plantillas json al directorio del juego.
# Declarar: propietario del juego, tick cero, nombre del juego.
# Inicializar y poblar la base de datos.
# Enviar mensajes al propietario y al bot de discord.
sub createGame {
  my $player = shift;
  my $name = shift;
  my $players = shift;
  my $template = readJsonFile($MODDIR."/assets/game_template.json");
  $template->{'game_name'}=$name;
  $template->{'game_owner'}=$player;
  $template->{'game_type'}=$players;
  my $room_state = readJsonFile($WDIR."/room_state.json");
  my @games = @{$room_state->{'games'}};
  push(@games, $name);
  $room_state->{'games'} = dclone(\@games);
  writeJsonFile($WDIR."/game_".$name.".json", $template);
  writeJsonFile($WDIR."/room_state.json", $room_state);
  callSendInfo($player, "*** Creando juego '".$name."' para ".$players." jugadores ***");
  # TODO: notificar a la sale del juego creado
}

1;
