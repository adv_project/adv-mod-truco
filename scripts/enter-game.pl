#!/usr/bin/env perl

# Registrar el jugador en la partida.
# Asignar el jugador a un equipo.
# Notificar a los demás jugadores.
# Actualizar la base de datos.
# Si el máximo de jugadores está completo, bloquear el ingreso de nuevos jugadores y notificar a los demás jugadores.
sub enterGame {
  my $player = shift;
  my $game = shift;
  callSendInfo($player, "*** Entras al juego '".$game."' (EN CONSTRUCCION) ***");
}

1;
