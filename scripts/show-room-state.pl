#!/usr/bin/env perl

# Identificar filtros en los argumentos, si los hubiera.
# Si no hay argumentos, aplicar los filtros por default o los presentes en la configuración del usuario.
# Construir y mostrar el estado de la sala.
sub showRoomState {
  my $player = $INPUT->{'player'};
  my @args = @{$INPUT->{'action'}}; # Filtros
  my @messages = ("Estado de la sala", "*** ESTE TEXTO ESTA EN CONSTRUCCION ***");
  callSendPlain($player, dclone(\@messages));
}

1;
