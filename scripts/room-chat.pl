#!/usr/bin/env perl

# Procesar el mensaje (aplicar filtros, colores, etc.).
# Transmitir el mensaje a todos los usuarios conectados que no tengan muteado el chat de sala en la configuración.
sub roomChat {
  my $player = $INPUT->{'player'};
  my $message = join(' ', @{$INPUT->{'action'}});
  my $room_state = readJsonFile($WDIR."/room_state.json");
  my @users = @{$room_state->{'users'}};
  for my $i (0 .. (@users-1)) {
    my $p = $users[$i];
    if (playerIsInRoom($p)) {
      sendRoomMessage($p, $player, $message);
    }
  }
}

1;
