#!/usr/bin/env perl

# Mostrar a la mesa las cartas de todos los jugadores, salvo que las hayan ocultado.
# Chequear, en caso de flor o envido querido, si los puntos fueron bien cantados. Caso contrario, penalizar al equipo infractor.
# Determinar el reparto de puntos.
# Actualizar la tabla.
# Actualizar la base de datos.
# Chequear si hay un ganador de la partida; de ser el caso, ejecutar scripts/finish-game.pl.
# Mostrar la tabla a la mesa.
# Resetear el estado de la partida y los jugadores.
# Iniciar la siguiente mano (scripts/deal-hand.pl).

1;
