#!/usr/bin/env perl

sub playerIsInRoom {
  my $player = shift;
  my $state = readJsonFile($WDIR."/user_".$player.".json");
  if ($state->{'state'} eq 'room') { return 1; }
  return 0;
}

sub gameExists {
  my $game = shift;
  if ($game eq "foo" or $game eq "bar") { return 1; }
  return 0;
}

sub gameHasAvailableSlots {
  my $game = shift;
  if ($game eq "bar") { return 1; }
  return 0;
}

sub sendRoomMessage {
  my $recipient = shift;
  my $sender = shift;
  my $message = shift;
  my $sender_state = readJsonFile($WDIR."/user_".$sender.".json");
  my $color = $sender_state->{'room-chat-color'};
  callSendPlain($recipient, $color."[".$sender."] ".$RESET.$message);
}

1;
