#!/usr/bin/env perl

use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

# File I/O

sub readFile {
	local $/;
	my $path = shift;
	return undef unless (-f $path);
	open my $fh, '<', $path;
	my $output = <$fh>;
	close $fh;
	return $output;
}

sub writeFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>', $path;
	print $fh $content;
	close $fh;
}

sub appendFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>>', $path;
	print $fh $content;
	close $fh;
}

sub copyFile {
  local $/;
  my $path = shift;
  my $dest = shift;
	unless (-f $path) { return undef; }
	if (-f $dest) { return undef; }
  my $content = readFile($path);
  writeFile($dest, $content);
}

# JSON

use JSON;

sub readJsonFile {
  my $path = shift;
  my $json = readFile($path);
  my $o = JSON::decode_json($json);
  return $o;
}

sub writeJsonFile {
  my $path = shift;
  my $o = shift;
  my $json = JSON->new->pretty(1)->encode($o);
  writeFile($path, $json);
}

# API

use Storable qw(dclone);

sub initializeOutput {
  writeFile($OUTPUTFILE, '[]');
}

sub outputPush {
	my $object = shift;
	my $json = readFile($OUTPUTFILE);
	my $out = JSON::decode_json($json);
	push @{ $out }, $object;
	$json = JSON->new->pretty(1)->encode( dclone($out) );
	writeFile($OUTPUTFILE, $json);
}

sub manage_error_badly {
	local $/;
	open my $fh, '>', $WDIR.'/error.json';
	print $fh qw<[]>;
	close $fh;
}

sub callSend {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message
	};
	outputPush($object);
}

sub callSendTagged {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $tag = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message,
		"tag" => $tag
	};
	outputPush($object);
}

sub callBroadcast {
  my $message_type = shift;
  my $message = shift;
  my $object = {
    "class"   => "broadcast",
    "type"    => $message_type,
    "message" => $message
  };
  outputPush($object);
}

sub callBroadcastSkipping {
  my $skip = shift;
  my $object = callBroadcast(shift, shift);
  $object->{'skip'} = $skip;
  outputPush($object);
}

sub callWrite {
  my $player = shift;
  my $action = shift;
  my $text = shift;
  my $object = {
    "class"  => "write",
    "player" => $player,
    "action" => $action,
    "text"   => $text
  };
  outputPush($object);
}

sub callSecondsPerTick {
  my $secspertick = shift;
  my $object = {
    "class"            => "seconds-per-tick",
    "seconds-per-tick" => $secspertick
  };
  outputPush($object);
}

sub callDescription {
  my $description = shift;
  my $object = {
    "class"       => "description",
    "description" => $description
  };
  outputPush($object);
}

sub callKick {
	my $player = shift;
	my $object = {
		"class"   => "kick",
		"player"  => $player
	};
	outputPush($object);
}

sub callLog {
	my $loglevel = int(shift);
	my $message = shift;
	my $object = {
		"class"   => "log",
		"log-level"  => $loglevel,
		"message" => $message
	};
	outputPush($object);
}

sub callPause {
	my $object = {
		"class"   => "pause"
	};
	outputPush($object);
}

sub callResume {
	my $object = {
		"class"   => "resume"
	};
	outputPush($object);
}

sub callSendPlain   { callSend(shift, "plain",   shift); }
sub callSendInfo    { callSend(shift, "info",    shift); }
sub callSendNotice  { callSend(shift, "notice",  shift); }
sub callSendWarning { callSend(shift, "warning", shift); }
sub callSendError   { callSend(shift, "error",   shift); }

sub callSendMePlain   { callSend($INPUT->{'player'}, "plain",   shift); }
sub callSendMeInfo    { callSend($INPUT->{'player'}, "info",    shift); }
sub callSendMeNotice  { callSend($INPUT->{'player'}, "notice",  shift); }
sub callSendMeWarning { callSend($INPUT->{'player'}, "warning", shift); }
sub callSendMeError   { callSend($INPUT->{'player'}, "error",   shift); }

sub callLogDebug    { callLog(7, shift); }
sub callLogInfo     { callLog(6, shift); }
sub callLogNotice   { callLog(5, shift); }
sub callLogWarn     { callLog(4, shift); }
sub callLogError    { callLog(3, shift); }
sub callLogCrit     { callLog(2, shift); }
sub callLogAlert    { callLog(1, shift); }
sub callLogEmerg    { callLog(0, shift); }

# Offline hooks
sub lock_offline_hook {
  my $i = 0;
  my $tries = 30;
  my $interval= 1;
  my $lockfile = $WDIR."/hook.lock";
  my $jsonfile = $WDIR."/hook.json";
  while (true) {
    $i = $i + 1;
    if ($i > $tries) {
      return 0;
    }
    if (-f $lockfile or -f $jsonfile) {
      sleep($interval);
      next;
    } else {
      writeFile($lockfile, '');
      last;
    }
  }
  return 1;
}

sub unlock_offline_hook {
  my $lockfile = $WDIR."/hook.lock";
  unlink($lockfile);
}

sub offlinePush {
	my $object = shift;
  my @array = @{$object};
  my $offlinehook = $WDIR.'/hook.json';
  my $json;
  if (-f $offlinehook) {
	  $json = readFile($offlinehook);
  } else {
    $json = '[]';
  }
	my $out = JSON::decode_json($json);
	my @offlinecalls = (@{ $out }, @array);
	$json = JSON->new->pretty(1)->encode( dclone(\@offlinecalls) );
	writeFile($offlinehook, $json);
}

sub runOfflineHook {
  my $a = shift;
  my @o = @{$a};
  my $i = 0;
  my $tries = 30;
  my $interval= 1;
  return 1 unless (lock_offline_hook());
  offlinePush(dclone(\@o));
  unlock_offline_hook();
  while (-f $WDIR.'/hook.json') {
    $i = $i + 1;
    if ($i > $tries) {
      return 0;
    }
    sleep($interval);
  }
  return 1;
}

sub callSendOffline {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message
	};
  my @offlinecalls = ();
  push(@offlinecalls, $object);
	runOfflineHook(dclone(\@offlinecalls));
}

sub callResumeOffline {
	my $object = {
		"class"   => "resume"
	};
  my @offlinecalls = ();
  push(@offlinecalls, $object);
	runOfflineHook(dclone(\@offlinecalls));
}

sub callManage {
  my $branch = shift;
  my $mod = shift;
  my $arguments = shift;
  my $object = {
    "class"   => "manage",
    "branch"  => $branch,
    "mod"  => $mod,
    "arguments"  => $arguments
  };
  outputPush($object);
}

sub readInput {
  $INPUT = readJsonFile($WDIR."/input.json");
}

# Colors
$RESET = "\0110";
$BOLD = "\0112";
$ITALIC = "\0113";
$UNDERLINE = "\0114";
$NOTBOLD = "\0115";
$NOTITALIC = "\0116";
$NOTUNDERLINE = "\0117";
$RED = "\011r";
$GREEN = "\011g";
$YELLOW = "\011y";
$BLUE = "\011b";
$MAGENTA = "\011m";
$CYAN = "\011c";
$WHITE = "\011w";
$POSITIVE = "\011p";
$NEGATIVE = "\011n";

# Random username color in room chat
sub generateNumberFromString {
  my $string = shift;
  my @chars = split(//, $string);
  my $c = 0;
  for my $i (0 .. (@chars-1)) {
    $c = $c + ord($chars[$i]);
  }
  my $m = $c % 13;
  return $m;
}

sub getUsernameColor {
  my $string = shift;
  my $n = generateNumberFromString($string);
  my $col;
  if ($n == 0) { $col=$RESET.$BOLD.$WHITE; }
  elsif ($n == 1) { $col=$RESET.$RED; }
  elsif ($n == 2) { $col=$RESET.$GREEN; }
  elsif ($n == 3) { $col=$RESET.$YELLOW; }
  elsif ($n == 4) { $col=$RESET.$BLUE; }
  elsif ($n == 5) { $col=$RESET.$MAGENTA; }
  elsif ($n == 6) { $col=$RESET.$CYAN; }
  elsif ($n == 7) { $col=$RESET.$BOLD.$RED; }
  elsif ($n == 8) { $col=$RESET.$BOLD.$GREEN; }
  elsif ($n == 9) { $col=$RESET.$BOLD.$YELLOW; }
  elsif ($n == 10) { $col=$RESET.$BOLD.$BLUE; }
  elsif ($n == 11) { $col=$RESET.$BOLD.$MAGENTA; }
  elsif ($n == 12) { $col=$RESET.$BOLD.$CYAN; }
  return $col;
}

1;
