#!/usr/bin/env perl

require $MODDIR."/scripts/create-game.pl";
require $MODDIR."/scripts/deal-hand.pl";
require $MODDIR."/scripts/enter-game.pl";
require $MODDIR."/scripts/exit-game.pl";
require $MODDIR."/scripts/finish-game.pl";
require $MODDIR."/scripts/finish-hand.pl";
require $MODDIR."/scripts/game-chat.pl";
require $MODDIR."/scripts/hide-cards.pl";
require $MODDIR."/scripts/play-blind.pl";
require $MODDIR."/scripts/play-card.pl";
require $MODDIR."/scripts/player-is-ready.pl";
require $MODDIR."/scripts/room-chat.pl";
require $MODDIR."/scripts/show-cards.pl";
require $MODDIR."/scripts/show-game-stats.pl";
require $MODDIR."/scripts/show-hand.pl";
require $MODDIR."/scripts/show-help.pl";
require $MODDIR."/scripts/show-room-state.pl";
require $MODDIR."/scripts/show-statistics.pl";
require $MODDIR."/scripts/swap-teams.pl";
require $MODDIR."/scripts/user-config.pl";
require $MODDIR."/scripts/user-game-config.pl";
require $MODDIR."/scripts/view-game.pl";
require $MODDIR."/scripts/welcome.pl";
require $MODDIR."/scripts/withdraw-player.pl";

1;
